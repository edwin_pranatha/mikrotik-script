# Apr 18th 2019
# checking dst-address 192.168.12.0/24 whether existing and using MT as gateway on ip route list menu, if not using MT or does not existed, change/create new
# this script can be used for all of version of routerboard and ROS
# this script can be used for all concession with one rule, the PPP client interface with MT the name is existing/created
# May 3rd 2019
# checking dst-address 192.168.222.0/24 in route list and change the gateway using MT interface
# AB

:global chkQ [/ip route find dst-address="192.168.12.0/24"]
:if ( $chkQ != "" ) do= {
/ip route remove [/ip route find dst-address="192.168.12.0/24"]
/ip route add disabled=no distance=1 dst-address=192.168.12.0/24 gateway=MT
:put "dstaddress 192.168.12.0/24 used MT as gateway"                           
} else={  
/ip route add disabled=no distance=1 dst-address=192.168.12.0/24 gateway=MT
:put "dstaddress 192.168.12.0/24 does not existed before but now existed"
}

:global chkt [/ip firewall nat find out-interface="mt"]
:if ( $chkt != "" ) do= {
/ip firewall nat set [find out-interface="mt"] out-interface="MT"
:put "out-interface with mt interface has been changed with MT"                           
} else={  
:put "out-interface with mt interface does not existed"
}

:global chkT [/ip firewall nat find out-interface="MT"]
:if ( $chkT != "" ) do= {
/ip firewall nat move [find out-interface="MT"] destination=17
:put "out-interface with MT interface has been moved"             
} else={  
:put "out-interface with MT interface does not existed"
}

:global chkfrmt [/ip firewall filter find out-interface="mt"]
:if ( $chkfrmt != "" ) do= {
/ip firewall filter set [find out-interface="mt"] out-interface="MT"
:put "out-interface with mt interface has been changed with MT on filter rules"                           
} else={  
:put "out-interface with mt interface on filter rules does not existed"
}

:if ( $chkT = "" &&  $chkt = "" ) do= {
/ip firewall nat
add action=masquerade chain=srcnat disabled=no out-interface=MT
:put "out-interface with MT interface does not existed before but now existed"
}

:global chkM [/ip route find dst-address="192.168.222.0/24"]
:if ( $chkM != "" ) do= {
/ip route set [/ip route find dst-address="192.168.222.0/24"] gateway=MT
:put "dstaddress 192.168.222.0/24 used MT as gateway"                           
} else={  
:put "dstaddress 192.168.222.0/24 does not existed"
}